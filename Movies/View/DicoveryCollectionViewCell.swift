//
//  DicoveryCollectionViewCell.swift
//  Movies
//
//  Created by Gustavo on 26/03/18.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import UIKit
import SDWebImage

class DicoveryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    
    func setup(movie:Movie){
        var imageURL = movie.poster_path
        if imageURL == nil{
            imageURL = ""
        }
        imageView.sd_setImage(with: NetworkURLs.Movie.image(fileName: imageURL!), placeholderImage: #imageLiteral(resourceName: "movie"), options: .cacheMemoryOnly, progress: nil, completed: nil)
        imageView.contentMode = .scaleAspectFit
        movieTitleLabel.text = movie.title
    }
}
