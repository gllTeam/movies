//
//  Genres.swift
//  Movies
//
//  Created by Gustavo on 21/03/18.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import Foundation

struct Genres: Codable {
    
    var id:Int
    var name: String
    
}
