//
//  Movie.swift
//  Movies
//
//  Created by Gustavo on 21/03/18.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import Foundation

struct Movie : Codable{
    
    var id: Int
    var home_page: String?
    var genres: [Genres]?
    var original_language: String
    var title: String
    var overview: String
    var backdrop_path: String?
    var poster_path: String?
    var release_date: String
    var vote_average: Float
    
}
