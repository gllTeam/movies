//
//  NetworkURLs.swift
//  Movies
//
//  Created by Gustavo on 22/03/18.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import Foundation

struct NetworkURLs{
    
    private static let kBaseURL = "https://api.themoviedb.org/3/"
    private static let kAPI_KEY = "de34a13151f61aaa51085fbf164f3918"
    
    struct Movie {
        static func detail(movieId:String) -> URL?{
            return URL.init(string: kBaseURL + "movie/\(movieId)?api_key=\(kAPI_KEY)")
        }
        
        static func search(query:String,page:Int) -> URL?{
            var urlComponents = URLComponents(string: kBaseURL + "search/movie")!
            urlComponents.queryItems = [
                URLQueryItem.init(name: "api_key", value: kAPI_KEY),
                URLQueryItem.init(name: "query", value: query),
                URLQueryItem.init(name: "page", value: "\(page)")
            ]
            return urlComponents.url
        }
        
        static func  discovery(page:Int,voteAverageGreater:Int) -> URL?{
            return URL.init(string: kBaseURL + "discover/movie?api_key=\(kAPI_KEY)&sort_by=popularity.desc&include_adult=false&vote_average.gte=\(voteAverageGreater)&page=" + "\(page)")
        }
        
        static func image(fileName:String) -> URL?{
            return URL.init(string: "https://image.tmdb.org/t/p/w500/" + fileName)
        }
    }
    
}
