//
//  NetworkManager.swift
//  Movies
//
//  Created by Gustavo on 23/03/18.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager{
    
    typealias RequestCompletion = (NetworkStatus) -> (Void)
    
    func getRequest(url:URL,completion:@escaping RequestCompletion){
        
        Alamofire.request(url, method: .get, parameters: nil).validate()
            .responseData { dataResponse in
                
                if let error = dataResponse.error {
                    completion(.failure(.defaultError(error)))
                    return
                }
                
                guard let data = dataResponse.data else {
                    completion(.failure(.noData))
                    return
                }
                
                completion(.success(data))
        }
        
    }
    
}
