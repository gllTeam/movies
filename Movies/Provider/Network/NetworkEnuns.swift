//
//  Enuns.swift
//  Movies
//
//  Created by Gustavo on 22/03/18.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case invalidURL
    case defaultError(Error)
    case noData
}

enum NetworkStatus{
    case failure(NetworkError)
    case success(Data)
}
