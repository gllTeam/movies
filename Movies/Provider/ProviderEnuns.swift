//
//  ProviderEnuns.swift
//  Movies
//
//  Created by Gustavo on 23/03/18.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import Foundation

enum ProviderStatus {
    case success(Data)
    case successMovie([Movie])
    case networkFailure(NetworkError)
    case storageError(Error)
    case defaultValue
}
