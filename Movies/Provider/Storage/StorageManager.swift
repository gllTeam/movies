//
//  StorageManager.swift
//  Movies
//
//  Created by Gustavo on 23/03/18.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import Foundation
import Disk

class StorageManager{
    
    func retrieveData(path:String) -> StorageStatus{
        let dataExists = Disk.exists(path, in: .temporary)
        if dataExists{
            do{
                let data = try Disk.retrieve(path, from: .temporary, as: Data.self)
                return .success(data)
            } catch{
                return .failure(error)
            }
        } else{
            return .noData
        }
    }
    
    func save(data:Data,path:String) -> Bool{
        do{
            try Disk.save(data, to: .temporary, as: path)
        } catch{
            return false
        }
        return true
    }
    
    func retrieveMovies(path:String) -> StorageStatus{
        let dataExists = Disk.exists(path, in: .documents)
        if dataExists{
            do{
                let movies = try Disk.retrieve(path, from: .documents, as: [Movie].self)
                return .successMovie(movies)
            } catch{
                return .failure(error)
            }
        } else{
            return .noData
        }
    }
    
    func appendMovie(movie:Movie,path:String) -> Bool{
        do{
            try Disk.append(movie, to: path, in: .documents)
        } catch{
            return false
        }
        return true
    }
    
}
