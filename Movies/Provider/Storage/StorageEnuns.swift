//
//  StorageEnuns.swift
//  Movies
//
//  Created by Gustavo on 23/03/18.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import Foundation

enum StorageStatus {
    case failure(Error)
    case noData
    case success(Data)
    case successMovie([Movie])
}
