//
//  MovieProvider.swift
//  Movies
//
//  Created by Gustavo on 22/03/18.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import Foundation
import Disk

class MovieProvider{
    
    typealias ProviderCompletion = (ProviderStatus) -> (Void)
    
    private let kMovieStorageKey = "movie"
    private let kMovieDiscoveryKey = "movieDiscovery"
    
    private let kFavoriteMoviesKey = "favorite/movies"
    
    func getDetail(movieId:String,completion:@escaping ProviderCompletion){
        let storagePath = kMovieStorageKey + "/" + movieId
        let storageResult = StorageManager().retrieveData(path: storagePath)
        switch storageResult{
            case .failure(let error):
                completion(.storageError(error))
                return
            case .success(let data):
                completion(.success(data))
                return
            case .noData:
                guard let url = NetworkURLs.Movie.detail(movieId: movieId) else{
                    completion(.networkFailure(.invalidURL))
                    return
                }
                NetworkManager().getRequest(url: url) { (status) -> (Void) in
                    switch status{
                    case .failure(let error):
                        completion(.networkFailure(error))
                        return
                    case .success(let data):
                        if !StorageManager().save(data: data, path: storagePath){
                            print("Error in persiste data.")
                        }
                        completion(.success(data))
                        break
                    }
                }
                break
            default:
                break
        }
    }
    
    func search(query:String,page:Int,completion:@escaping ProviderCompletion){
        guard let url = NetworkURLs.Movie.search(query: query, page: page) else{
            completion(.networkFailure(.invalidURL))
            return
        }
        NetworkManager().getRequest(url: url) { (status) -> (Void) in
            switch status{
                case .failure(let error):
                    completion(.networkFailure(error))
                    return
                case .success(let data):
                    completion(.success(data))
                    break
            }
        }
    }
    
    func discovery(page:Int,voteAverageGreater:Int,completion:@escaping ProviderCompletion){
        let storagePath = kMovieDiscoveryKey + "/" + "\(voteAverageGreater)" + "\(page)"
        let storageResult = StorageManager().retrieveData(path: storagePath)
        switch storageResult{
            case .failure(let error):
                completion(.storageError(error))
                return
            case .success(let data):
                completion(.success(data))
                return
            case .noData:
                guard let url = NetworkURLs.Movie.discovery(page: page,voteAverageGreater: voteAverageGreater) else{
                    completion(.networkFailure(.invalidURL))
                    return
                }
                NetworkManager().getRequest(url: url) { (status) -> (Void) in
                    switch status{
                    case .failure(let error):
                        completion(.networkFailure(error))
                        return
                    case .success(let data):
                        if !StorageManager().save(data: data, path: storagePath){
                            print("Error in persiste data.")
                        }
                        completion(.success(data))
                        break
                    }
                }
                break
            default:
                break
        }
    }
    
    func saveFavoriteMovie(movie:Movie) -> Bool{
        return StorageManager().appendMovie(movie: movie, path: kFavoriteMoviesKey)
    }
    
    func favoriteMovies() -> ProviderStatus{
        let storageResult = StorageManager().retrieveMovies(path: kFavoriteMoviesKey)
        switch storageResult {
            case .failure(let error):
                return .storageError(error)
            case .noData:
                return .successMovie([])
            case .successMovie(let movies):
                return .successMovie(movies)
            default:
                break
        }
        return .defaultValue
    }
}
