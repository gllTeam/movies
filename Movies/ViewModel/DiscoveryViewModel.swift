//
//  DiscoveryViewModel.swift
//  Movies
//
//  Created by Gustavo Luís Soré on 24/03/2018.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class DiscoveryViewModel{
    
    var page: Int
    var arrayMovies:[Movie] = []
    
    var movies = BehaviorRelay<[Movie]>(value: [])
    var movieOsbservable: Observable<[Movie]> {
        return self.movies.asObservable()
    }
    var businessErrorObservable = PublishSubject<MoviesResult>()
    let disposeBag = DisposeBag()
    
    init(){
        page = 1
    }
    
    func discoveryMovies(){
        MovieBusiness().discovery(page: self.page) { (movieResult) -> (Void) in
            switch movieResult{
            case .success(let arrayMovies):
                self.arrayMovies.append(contentsOf: arrayMovies)
                self.movies.accept(self.arrayMovies)
                self.page += 1
                break
            case .parseError:
                self.businessErrorObservable.onNext(.parseError)
            case .providerError(let error):
                self.businessErrorObservable.onNext(.providerError(error))
            }
        }
    }
}
