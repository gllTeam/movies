//
//  SearchViewModel.swift
//  Movies
//
//  Created by Gustavo Luís Soré on 24/03/2018.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class SearchViewModel{
    
    var page: Int
    var search: String
    
    var movies = BehaviorRelay<[Movie]>(value: [])
    var repositoriesObservable: Observable<[Movie]> {
        return self.movies.asObservable()
    }
    var businessErrorObservable = PublishSubject<MoviesResult>()
    let disposeBag = DisposeBag()
    
    init(search:String){
        page = 1
        self.search = search
    }
    
    func searchMovies(){
        MovieBusiness().search(query: self.search, page:self.page) { (movieResult) -> (Void) in
            switch movieResult{
            case .success(let arrayMovies):
                self.movies.accept(arrayMovies)
                break
            case .parseError:
                self.businessErrorObservable.onNext(.parseError)
            case .providerError(let error):
                self.businessErrorObservable.onNext(.providerError(error))
            }
            self.page += 1
        }
    }
}
