//
//  ViewController.swift
//  Movies
//
//  Created by Gustavo on 21/03/18.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        MovieBusiness().search(query: "beach boys", page: 1) { (result) -> (Void) in
            switch result{
            case .success(let movies):
                print(movies)
                break
            case .providerError(let error):
                print(error)
                break
            default:
                break
            }
        }
        MovieBusiness().discovery(page: 1) { (result) -> (Void) in
            switch result{
            case .success(let movies):
                print(movies)
                break
            case .providerError(let error):
                print(error)
                break
            default:
                break
            }
        }
        MovieBusiness().getDetail(movieId: "101") { (result) -> (Void) in
            switch result{
            case .success(let movies):
                print(movies)
                break
            case .providerError(let error):
                print(error)
                break
            default:
                break
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

