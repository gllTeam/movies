//
//  MovieParser.swift
//  Movies
//
//  Created by Gustavo on 22/03/18.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import Foundation

class MovieParser{
    
    struct Movies: Codable {
        let results: [Movie]
    }
    
    func parseMovieDetail(data:Data) -> Movie?{
        do {
            let result = try JSONDecoder().decode(Movie.self, from: data)
            return result
        } catch {
            print(error)
            return nil
        }
    }
    
    func parseMovies(data:Data) -> [Movie]{
        do {
            let result = try JSONDecoder().decode(Movies.self, from: data)
            return result.results
        } catch {
            print(error)
            return []
        }
    }
    
}
