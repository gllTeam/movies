//
//  MovieBusiness.swift
//  Movies
//
//  Created by Gustavo on 22/03/18.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import Foundation

class MovieBusiness{
    
    typealias MoviesCompletion = (MoviesResult) -> (Void)
    
    func getDetail(movieId:String,completion:@escaping MoviesCompletion){
        MovieProvider().getDetail(movieId: movieId) { (status) -> (Void) in
            switch status{
            case .networkFailure(let error):
                completion(.providerError(error))
                return
            case .storageError(let error):
                completion(.providerError(error))
                return
            case .success(let data):
                guard let movie = MovieParser().parseMovieDetail(data: data) else{
                    completion(.parseError)
                    return
                }
                completion(.success([movie]))
                break
            default:
                break
            }
        }
    }
    
    func search(query:String,page:Int,completion:@escaping MoviesCompletion){
        MovieProvider().search(query: query, page: page) { (status) -> (Void) in
            switch status{
            case .networkFailure(let error):
                completion(.providerError(error))
                return
            case .storageError(let error):
                completion(.providerError(error))
                return
            case .success(let data):
                let movies = MovieParser().parseMovies(data: data)
                completion(.success(movies))
                break
            default:
                break
            }
            
        }
    }
    
    func discovery(page:Int,completion:@escaping MoviesCompletion){
        MovieProvider().discovery(page: page,voteAverageGreater: 5) { (status) -> (Void) in
            switch status{
            case .networkFailure(let error):
                completion(.providerError(error))
                return
            case .storageError(let error):
                completion(.providerError(error))
                return
            case .success(let data):
                let movies = MovieParser().parseMovies(data: data)
                completion(.success(movies))
                break
            default:
                break
            }
        }
    }
    
    func saveFavoriteMovie(movie:Movie) -> Bool{
        return MovieProvider().saveFavoriteMovie(movie:movie)
    }
    
    func favoriteMovies() -> [Movie]{
        let result = MovieProvider().favoriteMovies()
        switch result {
            case .successMovie(let movies):
                return movies
            case .storageError(let error):
                print(error)
                return []
            default:
                return []
        }
    }
    
}
