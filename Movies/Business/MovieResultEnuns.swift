//
//  MovieResultEnuns.swift
//  Movies
//
//  Created by Gustavo on 22/03/18.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import Foundation

enum MoviesResult{
    case providerError(Error)
    case parseError
    case success([Movie])
}
