//
//  DiscoveryViewController.swift
//  Movies
//
//  Created by Gustavo on 26/03/18.
//  Copyright © 2018 Gustavo. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class DiscoveryViewController: UIViewController {
    
    @IBOutlet weak var collectionVIew: UICollectionView!
    
    var viewModel: DiscoveryViewModel!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Descoberta"
        
        self.viewModel = DiscoveryViewModel.init()
        self.collectionVIew.rx.setDelegate(self).disposed(by: self.disposeBag)
        bind()
        
        self.viewModel.discoveryMovies()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func bind(){
        
        self.viewModel.movieOsbservable
            .filter { !$0.isEmpty }
            .bind(to: self.collectionVIew.rx.items) { collectionVIew, index, element in
                guard let cell = collectionVIew.dequeueReusableCell(withReuseIdentifier: "DicoveryCollectionViewCell", for: IndexPath.init(row: index, section: 0)) as? DicoveryCollectionViewCell else {
                    return UICollectionViewCell()
                    
                }
                
                cell.setup(movie: element)
                
                return cell
            }
            .disposed(by: self.disposeBag)
        
        self.viewModel.movieOsbservable
            .skip(1)
            .subscribe(onNext: {
                
                if $0.isEmpty {
                    print("empty")
                }
            })
            .disposed(by: self.disposeBag)
        
        self.viewModel.movieOsbservable
            .subscribe(onNext: { _ in
                if self.viewModel.arrayMovies.count == 0 {
                    print("empty")
                }
            })
            .disposed(by: self.disposeBag)
        
    }
}

extension DiscoveryViewController : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row >= (self.viewModel.arrayMovies.count - 1) {
            self.viewModel.discoveryMovies()
        }
    }
}
